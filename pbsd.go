package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type DatabasesType struct {
	Host string
	User string
	Pass string
	Type string
	Name string
}

type BuildType struct {
	Pwd      string
	Os       string
	Oschroot string
	Pwdbuild string
	Pwdsrc   string
}

func main() {
	check_loop()
}

//Бесконечный цикл. Висит запущенный и мониторит статус
func check_loop() {
	for {
		chek_status()
		time.Sleep(5 * time.Second)
	}
}

//Функция запуска комманды в интерактивном режиме
func runCommand(cmdName string, arg ...string) {
	cmd := exec.Command(cmdName, arg...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	if err != nil {
		fmt.Printf("Failed to start. %s\n", err.Error())
		os.Exit(1)
	}
}

//Функция проверки статуса для начала сборки
func chek_status() {
	file, e := os.Open("config.json")

	if e != nil {
		fmt.Printf("File error: %v\n", e)
		os.Exit(1)
	}

	decoder := json.NewDecoder(file)
	resj := DatabasesType{}
	err := decoder.Decode(&resj)

	if err != nil {
		fmt.Println("error:", err)
	}

	//	fmt.Println(resj)
	//fmt.Println("Проверяем соединение с базой ")
	db, err := sql.Open(resj.Type, "xlin:tz70xxkeq@/mageia")

	if err != nil {
		fmt.Printf("Ошибка соединения. %s\n", err)
		panic(err.Error())
		return
	}
	//	defer db.Close()

	// Получаем данные о пакета, где статус 1 (сборка)
	// Формируем запрос к базе и получаем таблицу, где статутс пакета 1. Т.е. их и будем собирать
	rows, err := db.Query("select jobs.*, tsrc.* from jobs, tsrc where jobs.id = tsrc.id and jobs.status = 1;")
	if err != nil {
		panic(err.Error())
		return
	}

	for rows.Next() {
		var fid int
		var file_id int
		var platform_id int
		var status int
		var sid int
		var user_id int
		var filename string
		var description string
		err = rows.Scan(&fid, &file_id, &platform_id, &status, &sid, &user_id, &filename, &description)
		if err != nil {
			panic(err.Error())
			return
		}
		if status == 1 {
			fmt.Println(">>> Обнаружен пакет для сборки! <<<")
			file_build, e := os.Open("build.json")

			if e != nil {
				fmt.Printf("File error: %v\n", e)
				os.Exit(1)
			}

			decoderBuild := json.NewDecoder(file_build)
			resjb := BuildType{}
			err := decoderBuild.Decode(&resjb)
			if err != nil {
				fmt.Println("error:", err)
			}
			// Собираем пакет на основе созданного файла json на основе выбора пользователя
			runCommand("docker",
				"run",
				"--rm",
				"-it",
				"-v",
				resjb.Pwd,
				resjb.Os,
				"/root/mageia-chroot.rpmbuild",
				"-t",
				resjb.Oschroot,
				"-o",
				resjb.Pwdbuild,
				resjb.Pwdsrc)
			rows, err := db.Prepare("update jobs set status=? where status=? and id=?")
			if err != nil {
				panic(err.Error())
				return
			}
			// После сборки пакета меняем статус 1 на 2
			res, err := rows.Exec("2", "1", fid)
			if err != nil {
				panic(err.Error())
				return
			}
			affect, _ := res.RowsAffected()

			fmt.Println("Статус ", affect)
			//			fmt.Println(res)
		}
		defer db.Close()
	}
}
